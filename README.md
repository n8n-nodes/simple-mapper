![Banner image](https://user-images.githubusercontent.com/10284570/173569848-c624317f-42b1-45a6-ab09-f0ea3c247648.png)

# Simple Mapper Node for n8n

## Description

The "Simple Mapper" node allows users to map input values to desired output values based on a set of predefined key-value pairs.

You can set mappings through either the standard interface or the advanced JSON editor. If mappings are set in both, the node will prioritize the mappings provided in the advanced JSON editor. Ensure you only use one method for setting the mappings.


## Features

- **Mapping**: Quickly map input values to output using the predefined mappings.
- **Flexibility**: Add as many key-value pairs as required.

## Usage

1. Add the Simple Mapper node to your n8n workflow.
2. Configure the node by adding your desired key-value pairs.
3. Connect the node to your input and output nodes.
4. Run the workflow and watch the transformation happen!

## Examples

If you set the node's mapping to:
```
{
'red': '#FF0000',
'mint': '#DDFFE7'
...
}
```
An input with the value "red" will produce an output with the value "#FF0000".


use interface properties

![useInterface.png](https://gitlab.com/n8n-nodes/simple-mapper/-/raw/main/assets/useInterface.png?ref_type=heads)

advanced JSON editor

![inputJson.png](https://gitlab.com/n8n-nodes/simple-mapper/-/raw/main/assets/inputJson.png?ref_type=heads)

for create new field in output

![inputJsonButCreateNewField.png](https://gitlab.com/n8n-nodes/simple-mapper/-/raw/main/assets/inputJsonButCreateNewField.png?ref_type=heads)






## Support

For support, issues, or feature requests, please open an issue on our GitHub repository.

## License



