import {
  IExecuteFunctions,
} from 'n8n-core';
import {
    INodeExecutionData,
    INodeType,
    INodeTypeDescription,
    NodeOperationError
} from 'n8n-workflow';

function getValue(keyString: string,item: any): any{

    const keyArray :any =keyString.split('.');
    let mappedValue: any
    if('json' in item){
        const data= item['json'];
        mappedValue = {...data};
        let k :any;
        while( k = keyArray.shift()  )
        {
            if(typeof mappedValue == 'object' && k in mappedValue )
                mappedValue=mappedValue[k] ||''
            else
                mappedValue=''
        }
    }
    return mappedValue
};
function setValue(keyString: string,value: any,item: any): any{

    const keyArray :any =keyString.split('.');
    let mappedValue: any
    if('json' in item){
        const data= item['json'];
        mappedValue = data;
        let k :any;
        while( k = keyArray.shift()  )
        {
            if(typeof mappedValue == 'object' && k in mappedValue ){
                if(!keyArray.length) {
                    mappedValue[k] = value
                }
                else
                    mappedValue=mappedValue[k] ||''

            }

        }
    }

};
function newValue(keyString: string,value: any,item: any): any{

    const keyArray :any =keyString.split('.');
    let mappedValue: any
    if('json' in item){
        const data= item['json'];
        mappedValue = data;
        let k :any;
        while( k = keyArray.shift()  )
        {
            if(typeof mappedValue == 'object' && k in mappedValue ){

                if(!keyArray.length)
                    mappedValue[k]=value
                else
                    mappedValue=mappedValue[k] ||''
            }else{
                if(typeof mappedValue == 'object' && !keyArray.length)
                    mappedValue[k]=value
            }

        }
    }
    return mappedValue
};
export class Mapper implements INodeType {
  description: INodeTypeDescription = {
      displayName: 'Simple mapper',
      name: 'mapper',
      icon: 'file:Mapper.svg',
      group: ['transform'],
      version: 1,
      description: 'Map input keys to specific values',
      defaults: {
          name: 'Simple Mapper',
      },
      inputs: ['main'],
      outputs: ['main'],
      properties: [
          {
              displayName: 'Key to Map',
              name: 'key_to_map',
              type: 'string',
              default: '',
          },
          {
              displayName: 'New Key or Replace Key if Empty',
              name: 'new_key',
              type: 'string',
              default: '',
          },
          {
              displayName: 'Mappings',
              name: 'mappings',
              type: 'fixedCollection',
              default: {},
              typeOptions: {
                  multipleValues: true,
              },
              options: [
                  {
                      name: 'mapItem',
                      displayName: 'Map Item',
                      values: [
                          {
                              displayName: 'Key',
                              name: 'key',
                              type: 'string',
                              default: '',
                              description: 'The key to map from',
                          },
                          {
                              displayName: 'Value',
                              name: 'value',
                              type: 'string',
                              default: '',
                              description: 'The value to map to',
                          },
                      ],
                  },
              ],
          },
          {
              displayName: 'Advanced Mappings (JSON)',
              name: 'advancedMappings',
              type: 'json',
              default: '{}',
              description: 'Input your mappings in JSON format. This will override the standard mappings.',
              placeholder: '{"red": "#ff0000", "mint": "#DDFFE7"}'
          }
      ],
  };

  async execute(this: IExecuteFunctions): Promise<INodeExecutionData[][]> {

      const returnData: INodeExecutionData[] = [];
      const items = this.getInputData();
      const keyString = this.getNodeParameter('key_to_map', 0) as string;
      const newKey = this.getNodeParameter('new_key', 0) as string;

      const advancedMappingsRaw = this.getNodeParameter('advancedMappings', 0) as string;
      let mappings: { [key: string]: string } = {};

      if (advancedMappingsRaw && advancedMappingsRaw !== '{}') { // Checking if advanced mapping is set and not just an empty JSON.
          try {
              mappings = JSON.parse(advancedMappingsRaw);
          } catch (error) {
              throw new NodeOperationError(this.getNode(), 'Invalid JSON format for advanced mappings.');
          }
      } else {
          const arrayMappings = this.getNodeParameter('mappings', 0) as {
              mapItem: Array<{ key: string; value: string }>
          };
          mappings = arrayMappings.mapItem.reduce((acc, item) => {
              acc[item.key] = item.value;
              return acc;
          }, {} as Record<string, string>);

      }



      for (let i = 0; i < items.length; i++) {
          const item = items[i];
          let mappedValue: any = "";
          const inputValue: any = getValue(keyString,item);


          mappedValue=mappings[inputValue]

          if (mappedValue ) {
              if(newKey !==''){
                  newValue(newKey,mappedValue,item)
              }else{
                  setValue(keyString,mappedValue,item)
              }

          }
          returnData.push(item)
      }

      return [returnData];
  }
}
